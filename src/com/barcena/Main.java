package com.barcena;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        System.out.println("--Activity 2--");

        // appScanner
        Scanner appScanner = new Scanner(System.in);

        // values
        int askYear;
        boolean leap;

        System.out.println("What year were you born?");
        askYear = appScanner.nextInt();

        if (askYear % 4 == 0) {
            if (askYear % 100 == 0) {
                if (askYear % 400 == 0)
                    leap = true;
                else
                    leap = false;

            }
            else
                leap = true;
        }
        else
            leap = false;

        if (leap)
            System.out.println(askYear + " is a leap year.");
        else
            System.out.println(askYear + " is not a leap year.");
    }
}
